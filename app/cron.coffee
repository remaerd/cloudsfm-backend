CronJob 	= require('cron').CronJob

exports.init = ->
	daily = new CronJob
		cronTime: '00 00 00 * * *'
		onTick: ->
			cronTask('daily')
		start: false

	daily.start()

cronTask = (type) ->
	console.log type