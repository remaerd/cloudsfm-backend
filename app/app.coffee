
############################################## 
# 
#  Clouds.FM API 后端服务器
#  2013 ／ SEAN CHENG
# 
##############################################

_ 				= require('underscore')
express 	= require('express')
request 	= require('request')
moment		= require('moment')
validator	= require('express-validator')

db 				= require('./database')
cron     	= require('./cron')
app				= express()
redis   	= null

## EXPRESSJS 设置 ############################

app.configure 'development', ->
	app.locals.cors = 'http://studio.dev'
	app.locals.paypalLoginURL = 'https://api.sandbox.paypal.com/v1/identity/openidconnect/userinfo'
	redis = require('redis').createClient()
	db.init('mongodb://localhost:27017/cloudsfm')
	app.use express.errorHandler()
	app.use express.cookieParser('7bxwW8YFzojrHgB')
	app.use express.session {secret: '7bxwW8YFzojrHgB', cookie: {maxAge: 31536000}}

app.configure 'production', ->
	app.locals.cors = 'http://studio.clouds.fm'
	app.locals.paypalLoginURL = 'https://api.paypal.com/v1/identity/openidconnect/userinfo'
	redis = require('redis').createClient(6379, 'nodejitsudb5298909686.redis.irstack.com')
	redis.auth_pass = 'nodejitsudb5298909686.redis.irstack.com:f327cfe980c971946e80b8e975fbebb4'
	store = require('connect-redis')(express) 
	db.init('mongodb://clodusfm-studio:ButuUnE{PT42cAW@ds031617.mongolab.com:31617/cloudsfm')
	app.use express.cookieParser('7bxwW8YFzojrHgB')
	app.use express.session {store:new store {client:redis}, secret:'7bxwW8YFzojrHgB', cookie: {maxAge: 31536000}}
	app.use require('raven').middleware.express 'https://13ad93150e5f4de0824beddb9cfaadc2:c086b8bf571e47c8bf6855443ad715cc@app.getsentry.com/12274'

app.configure ->
	app.use express.compress()
	app.use express.methodOverride()
	app.use express.bodyParser()
	app.use validator()
	app.use app.router
	app.use express.csrf()

## 通用接口 ##################################

# 防止 CSRF 跨域伪造
csrf = (req,res,next) ->
	res.locals.token = req.session._csrf
	next()

app.all '/studio/*', csrf, (req,res,next) ->
	res.header('Access-Control-Allow-Headers', 'content-type')
	res.header('Access-Control-Allow-Origin', app.locals.cors)
	res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE')
	res.header('Access-Control-Allow-Credentials', 'true')
	next()

# 根据数据类型生成 Rackspace Cloud Files 上传签名及参数
uploadSignature = (type) ->
	switch req.params.type
		when 'image' then templates = '72947790706a4a029ba7410960ca7b3f'
		when 'audio' then templates = 'eb0f07b700804b51b5223c0b870c5424'

	authKey = '57d01f7ebb624ddaa9bf9d5623a0e6c2'
	authSecret = 'aad5ff1695f6342d6e9e7df9e7900bdcaa26b089'

	params = 
		auth: 
			key: authKey
			expires: moment.utc().add('minutes', 15).format("YYYY\/MM\/DD HH:mm:ssZ")
		template_id: templates

	paramsString = JSON.stringify(params)
	
	return result =
		signature: crypto.createHmac('sha1',authSecret).update(paramsString).digest('hex')
		params: paramsString

# 登录验证
isLoggedIn = (req,res,next) ->
	distributor = req.session.current_distributor
	if distributor isnt req.cookies.distributor or distributor is undefined then res.json 401, {error: 'Not Logged In'}
	else next()

# 错误反馈

error400 = (res,err) -> res.json(code,err)
error500 = (res,err) -> res.json(code,err)

errorHandler(err,req,res,code,next) ->
	res.status(code)
	res.json(code,err)

## SHOWSTUDIO 接口 ##########################

# 从 Cookies 中获取当前用户 ID
app.get 'studio/session', isLoggedIn, (req,res) ->
	req.session.current_distributor = req.cookies.distributor
	db.distributor.findById req.session.current_distributor
	res.json(req.cookies.distributor)

# 登录帐号，如帐号不存在，根据 PAYPAL 返回的参数创建新帐号
app.get '/studio/login', (req,res) ->
	req.assert('access_token','Paypal Access Token Required').notEmpty()
	if req.validationErrors() then error400(res,req.validationErrors(true)) else
		data = request
			uri: app.locals.paypalLoginURL
			headers: {'Authorization': 'Bearer ' + req.body.access_token}
			(err,response,body) ->
				if err or response.statusCode >= 300 then error400(res, 400, 'Unable to get User Info from PayPal Server')
				else
					# TODO: 完善通过 PayPal 登录流程
					console.log body
					db.distributor.findOne {email:body.email}, (err,distributor) ->
						if not distributor
							db.distributor.create
								email: body.email
								name: body.name
								created_at: Date.now()
								(err,distributor) ->
									if not err
										req.session.current_distributor = distributor._id
										res.cookie('distributor',distributor._id)
										res.json
											distributor: distributor.toJSON()
											isNew: false
						else
							req.session.current_distributor = distributor._id
							res.cookie('distributor',distributor._id)
							res.json
								distributor: distributor.toJSON()
								isNew: true

# 登出帐号，删除 Session 和 Cookie
app.get '/studio/logout', isLoggedIn, (req,res) ->
	req.session = null
	res.clearCookie('distributor')
	res.send(200)
		
# 设置帐号
app.put '/studio/me', isLoggedIn, (req,res) ->
	req.assert('name').notEmpty()
	if req.validationErrors() then error400(res,req.validationErrors(true)) else
		db.distributor.findById req.session.distributor, (err,distributor) ->
			if err then error400(res,err) else
				data = {}
				data.name = req.query.name
				if req.body.company_name then data.company_name = req.body.company_name
				distributor.update data, (err,response) ->
					if err then error400(res,err) else
						req.session.current_distributor = response._id
						res.json({success: response})

# 获取发布者的全部节目列表
# TODO 优化列表的数据，只获取节目名及封面图
app.get '/studio/shows', isLoggedIn, (req,res) ->
	db.show.find {distributor: req.session.current_distributor}.select('-preferences -scripts.value -materials').exec(err, shows) ->
		if err then error400(res,err) else res.json(shows)

# 获取发布者的节目
app.get '/studio/shows/:show_id', isLoggedIn, (req,res) ->
	db.show.findById req.params.show_id, (err,show) ->
		if err then error400(res,err) else res.json(show)

# 创建新节目
app.post '/studio/shows', isLoggedIn, (req,res) ->
	db.show.create
		distributor: req.session.current_distributor
		scripts: [{title: 'Untitled Show', language: 'eng'}]
		(err,show) ->
			if err then res.json(400,error) else res.json(201, show)

# 更新节目
app.put '/studio/shows/:id', isLoggedIn, (req,res) ->
	newValues = req.body
	if newValues is null then error400(res,'Imcomplete Data') else
		newValues.updated_at = Date.now()
		for script in newValues.scripts
			short_titles = script.short_title.toLowerCase().split(' ')
			tags = script.tags.toLowerCase().split(' ')
			keywords = _.union(keywords,tags,short_titles,show.distributor.name.toLowerCase())
			script.keywords = keywords
	
		db.show.findByIdAndUpdate req.params.id, json ,(err,show) ->
			if err then res.json(400,error) else res.json(show)

# 发布节目
app.get '/studio/shows/:id/publish', isLoggedIn, (req,res) ->
	db.show.findById req.params.id, (err,show) ->
		if err then res.json(400,error) else
			if show.status = 'unpublished' then show.status = 'published'
			else if show.status = 'published' then show.status = 'unpublished'
			else res.json(400,'Your Show is under strange situation.')
			show.push_updated_at = Date.now()
			show.save (err,response) -> if err then res.json(400,error) else res.json(response)

# 节目更新推送
app.get '/studio/shows/:id/push', isLoggedIn, (req,res) ->
	db.show.update {_id:req.params.id, distributor:req.session.distributor}, {push_updated_at: Date.now()} , (err) ->
		if err then error400(res, error) else res.json(200)

# 删除节目
app.del '/studio/shows/:id', isLoggedIn, (req,res) ->
	db.show.remove {_id:req.params.id, distributor:req.session.distributor}, (err) ->
		if err then res.json(400,error) else res.json(200)

# 获得用户的材料列表
app.get '/studio/materials/:page', isLoggedIn, (req,res) ->
	db.material.find({distributor: req.session.distributor}).skip(req.params.page * 20).limit(20).exec(err,show) ->
		if err then res.json(400,error) else res.json(200)

# 新建材料
app.post '/studio/materials', isLoggedIn, (req,res) ->
	db.material.create
		type: req.body.type
		uid: req.body.uid
		values: req.body.values
		distributor: req.session.distributor
		created_at: Date.now()
		updated_at: Date.now()
		(err,distributor) ->
			if not err
				req.session.current_distributor = distributor._id
				res.cookie('distributor',distributor._id)
				res.json
					distributor: distributor.toJSON()
					isNew: false

# 更新材料
# 增强对 Request 中的 Body 数据进行验证
app.put '/studio/materials/:id', isLoggedIn, (req,res) ->
	req.assert('type','').notEmpty()
	if req.validationErrors() then error400(res,req.validationErrors(true)) else
		newValues =
			type: req.body.type
			uid: req.body.uid
			values: req.body.values
			updated_at: Date.now()
		db.show.findOneAndUpdate {_id:req.params.id, distributor:req.session.distributor}, newValues ,(err,show) ->
			if err then error400(res, 400, err) else res.json(show)

# 删除材料
app.del '/studio/materials/:id', isLoggedIn, (req,res) ->
	db.material.remove {_id:req.params.id, distributor:req.session.distributor}, (err) ->
		if err then error400(res, 400, err) else res.json(200)

# 借助服务器进行第三方服务访问
# TODO: 完善对 POST, GET, UPDATE, DELETE 等 Method 支持；增强对返回对象的解释能力
app.post 'studio/fetch', isLoggedIn, (req,res) ->
	if not req.body.url then error400(res,'Imcomplete Data') else
		url = req.body.url
		if url.match('http://') == null and url.match('https://') == null then url = 'http://'.concat(url)
		data = request
			uri: url
			(err,response,body) ->
				if err or body.error then res.json(400,body + error)
				else
					content_type = response.headers['content-type']
					if content_type.match('application/xml') != null then res.type('application/xml')
					if content_type.match('application/json') != null then res.type('application/json')
					res.send(body)

# 生成 Rackspace 上传 Token
app.get '/studio/upload/', isLoggedIn, (req,res) ->
	req.assert('type','Type value Required. Must be audio or image')
	res.json uploadSignature()

## SHOWSTORE 接口 ###########################

# 获取节目列表
# TODO 优化返回数据，只返回封面，节目标题／介绍；可指定获取语言类型；对数据进行缓存处理
app.get '/store/shows', (req,res) ->
	values = req.query
	if values.distributor_id is null and values.tag is null and values.keyword is null
		error400(res,'One of these attributes must not be null: distributor_id, tag, keyword')
	else
		conditions = {status: 'published'}
		conditions.languages = values.languages.split() if values.languages
		conditions.distributor = values.distributor_id if values.distributor_id
		
		db.show.find(conditions).skip(req.query.page * 20).limit(20)
		.select('-preferences -scripts.value -materials -daily_metrics -weekly_metrics -monthly_metrics -yearly_metrics').exec(err,shows) ->
			if err then error400(res,err) else res.json(shows)

# 获取节目
# TODO 优化返回数据；对数据进行缓存处理
app.get '/store/show', (req,res) ->
	req.assert('show_id','Show ID Required').notNull()
	db.show.findById(req.query.show_id).exec (err,shows) ->
		if err then error400(res,err) else res.json(shows)	

# 获取更新节目
# TODO 优化返回数据
app.post '/store/updates', (req,res) ->
	if req.body is null then error400(res,'Imcomplete Data') else
		json = _.uniq(req.body)
		ids = _.keys[json]
		result = []

		db.show.find({_id: {$in: ids}}).select('-daily_metrics -weekly_metrics -monthly_metrics -yearly_metrics').exec(err,shows) ->
			if err then error400(res,err) else
				for show in shows
					if show['push_updated_at'] isnt json[show['id']]
						result.push(show)
					res.json(result)

## SHOWRANK 接口 ############################

# 启动软件时，纪录启动次数
app.get '/rank/app/start', (req,res) ->
	redis.incr('APP_START')

# 节目广告结束后进行纪录
app.get '/rank/show/:show_id/start', (req,res) ->
	redis.incr(req.params.type)

# 节目广告结束后进行纪录
app.get '/rank/show/:show_id/finish', (req,res) ->
	redis.incr('req')

## 运行 #####################################

app.listen(3000)
cron.init()
console.log 'Server is now running!'

## END ######################################
	